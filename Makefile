docker-deps:
	python3.8 -m pip download -d deps -r requirements.txt -r requirements.dev.txt

docker-image: docker-deps
	docker build -t yawf/identification/telegram -f Docker/Dockerfile .
