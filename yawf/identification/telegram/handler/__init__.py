import aiohttp.web

from yawf.identification.telegram.handler import oauth


async def _ping_handler(_):
    return aiohttp.web.json_response({'ping': 'pong'})


def get_routes(oauth_handler: oauth.TelegramOAuthHandler,
               ping=_ping_handler):
    return [
        aiohttp.web.post('/ping', ping),
        aiohttp.web.post('/oauth/telegram', oauth_handler.authenticate),
    ]
