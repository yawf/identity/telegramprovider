import datetime
import hashlib
import hmac
import json
import uuid

import aiohttp.web
import jwt

from yawf.utils.exception import AppHTTPException

from yawf.identification.telegram import config
from yawf.identification.telegram.repository import tg_ids_repo


class TelegramOAuthHandler:
    def __init__(self, id_settings: config.IdentificationSettings,
                 trust_settings: config.TrustSettings,
                 tg_ids_repository: tg_ids_repo.TelegramIdsRepository):
        self._id_settings = id_settings
        self._trust_settings = trust_settings
        self._tg_ids_repo = tg_ids_repository

    async def authenticate(self, request: aiohttp.web.Request):
        try:
            body = await request.json()
        except json.JSONDecodeError:
            raise AppHTTPException(
                status=aiohttp.web.HTTPBadRequest,
                code='MALFORMED_TELEGRAM_CLAIMS',
            )

        try:
            tg_hash = body.pop('hash')
        except KeyError:
            raise AppHTTPException(
                status=aiohttp.web.HTTPBadRequest,
                code='MALFORMED_TELEGRAM_CLAIMS',
            )

        required_keys = {'id', 'first_name', 'last_name', 'username', 'auth_date'}
        if not set(body.keys()).issuperset(required_keys):
            raise AppHTTPException(
                status=aiohttp.web.HTTPBadRequest,
                code='MALFORMED_TELEGRAM_CLAIMS',
            )

        try:
            auth_timestamp = datetime.datetime.fromtimestamp(body['auth_date'], datetime.timezone.utc)
        except TypeError:
            raise AppHTTPException(
                status=aiohttp.web.HTTPBadRequest,
                code='MALFORMED_TELEGRAM_CLAIMS',
            )

        signature = calculate_hash(body, self._id_settings.tg_bot_token.encode())

        if signature.lower() != tg_hash.lower():
            raise AppHTTPException(
                status=aiohttp.web.HTTPForbidden,
                code='INVALID_TELEGRAM_CLAIMS',
            )

        try:
            external_id = await self._tg_ids_repo.authenticate(
                tg_ids_repo.TelegramId(tg_id=body['id'], tg_username=body['username']),
                auth_timestamp,
            )
        except tg_ids_repo.ReusedAuthenticationAttempt:
            raise AppHTTPException(
                status=aiohttp.web.HTTPForbidden,
                code='DUPLICATE_AUTHENTICATION_ATTEMPT',
            )

        display_name = ' '.join([body['first_name'], body['last_name']])
        identification_token = self._issue_id_token(external_id, display_name, self._id_settings.id_token_lifetime)

        return aiohttp.web.json_response(
            {
                'idToken': identification_token,
                'expiresIn': int(self._id_settings.id_token_lifetime.total_seconds()),
            }
        )

    def _issue_id_token(self, external_id, display_name, expires_in: datetime.timedelta):
        return jwt.encode(
            {
                'iss': self._trust_settings.issuer_id,
                'sub': '**',
                'act': {'*': ['*']},
                'exp': int((datetime.datetime.now(datetime.timezone.utc) + expires_in).timestamp()),
                'jti': str(uuid.uuid4()),

                'yawf.io/externalId': 'tg+id://' + external_id,
                'yawf.io/externalDisplayName': display_name,
            },
            key=self._trust_settings.private_key,
            algorithm='RS256',
        ).decode()


def calculate_hash(payload, bot_token):
    digest_parts = sorted(payload.items(), key=lambda item: item[0])

    digest_value = '\n'.join(
        [
            '{}={}'.format(key, value)
            for key, value in digest_parts
        ]
    )
    return hmac.new(
        hashlib.sha256(bot_token).digest(),
        msg=digest_value.encode(),
        digestmod=hashlib.sha256,
    ).hexdigest()
