import datetime

import bson
import pymongo.errors

import motor.core
import typing

from yawf.identification.telegram.repository import tg_ids_repo
from yawf.identification.telegram.repository.tg_ids_repo import ExternalIdentifier, TelegramId


class MongoTelegramIdsRepository(tg_ids_repo.TelegramIdsRepository):
    def __init__(self, tg_ids_collection: motor.core.AgnosticCollection):
        self._collection = tg_ids_collection

    async def authenticate(self, tg_id: TelegramId, auth_timestamp: datetime.datetime) -> ExternalIdentifier:
        try:
            result = await self._collection.find_one_and_update(
                {'tg_id': tg_id.tg_id,
                 'last_auth': {'$lt': auth_timestamp}},
                {
                    '$set': {
                        'tg_username': tg_id.tg_username,
                        'last_auth': auth_timestamp,
                    }
                },
                upsert=True, return_document=pymongo.ReturnDocument.AFTER,
            )
        except pymongo.errors.DuplicateKeyError:
            raise tg_ids_repo.ReusedAuthenticationAttempt

        return str(result['_id'])

    async def get_ids(self, external_id: ExternalIdentifier) -> typing.Optional[TelegramId]:
        doc = await self._collection.find_one(
            {'_id': bson.ObjectId(external_id)},
        )

        if not doc:
            return None

        return TelegramId(
            tg_id=doc['tg_id'],
            tg_username=doc['tg_username'],
        )
