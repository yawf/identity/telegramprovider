import abc
import datetime
import typing


ExternalIdentifier = str


class ReusedAuthenticationAttempt(BaseException):
    pass


class TelegramId(typing.NamedTuple):
    tg_id: str
    tg_username: str


class TelegramIdsRepository(abc.ABC):
    @abc.abstractmethod
    async def authenticate(self, tg_id: TelegramId, auth_timestamp: datetime.datetime) -> ExternalIdentifier:
        raise NotImplementedError

    @abc.abstractmethod
    async def get_ids(self, external_id: ExternalIdentifier) -> typing.Optional[TelegramId]:
        raise NotImplementedError
