import datetime
import json
import os
import typing


class IdentificationSettings(typing.NamedTuple):
    tg_bot_token: str
    id_token_lifetime: datetime.timedelta


class TrustSettings(typing.NamedTuple):
    issuer_id: str
    private_key: bytes


class MongoConfig(typing.NamedTuple):
    mongo_url: str


class AppConfig(typing.NamedTuple):
    id_settings: IdentificationSettings
    trust_settings: TrustSettings
    mongo_config: MongoConfig


def load_app_config(config_file: str):
    with open(config_file, 'r') as fp:
        return parse_app_config(json.load(fp), os.path.dirname(config_file))


def parse_app_config(config_data, context_dir: str):
    identification = config_data['identification']
    trust = config_data['trust']
    mongo = config_data['mongo']

    private_key = trust['privateKey']
    if not os.path.isabs(private_key):
        private_key = os.path.join(context_dir, private_key)

    with open(private_key, 'rb') as fp:
        private_key = fp.read()

    bot_token = identification['botToken']
    if bot_token.startswith('$file://'):
        bot_token = bot_token[len('$file://'):]
        with open(bot_token, 'r') as fp:
            bot_token = fp.read()

    return AppConfig(
        id_settings=IdentificationSettings(
            tg_bot_token=bot_token,
            id_token_lifetime=datetime.timedelta(seconds=identification['tokenLifetime']),
        ),
        trust_settings=TrustSettings(
            issuer_id=trust['issuerId'],
            private_key=private_key,
        ),
        mongo_config=MongoConfig(
            mongo_url=mongo['url'],
        )
    )
