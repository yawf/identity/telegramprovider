import argparse
import asyncio

from yawf.identification.telegram import config, di


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True)
    parser.add_argument('--port', default=8080)

    args = parser.parse_args()

    conf = config.load_app_config(args.config)

    asyncio.run(di.run_app(conf, args.port))


if __name__ == '__main__':
    main()
