import contextlib
import os

import aiohttp.web

import motor.motor_asyncio

import spydi.context
from spydi.context import FactoryType, BindingScope
import spydi.factory
import spydi.tuple_destructuring


from yawf.utils import exception

from yawf.identification.telegram import config, handler
from yawf.identification.telegram.repository import mongo_tg_ids_repo


def make_context(conf: config.AppConfig):
    ctx = spydi.context.DependencyContext()
    ctx.add_factory(spydi.tuple_destructuring.TupleDestructuringFactory(config.AppConfig))

    ctx.bind(conf, to=config.AppConfig, scope=BindingScope.SINGLETON)
    ctx.bind(mongo_tg_ids_repo.MongoTelegramIdsRepository)
    ctx.bind_factory(_make_mongo_collection, to='tg_ids_collection', factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(_make_middlewares, to='middlewares', factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(handler.get_routes, to='routes', factory_type=FactoryType.SIMPLE)

    return ctx


def _make_middlewares():
    return [exception.json_exception_middleware]


def _make_mongo_collection(mongo_conf: config.MongoConfig):
    mongo_password = os.getenv('MONGO_PASSWORD')

    auth_kwargs = {}
    if mongo_password:
        auth_kwargs.update({
            'username': os.environ['MONGO_USER'],
            'password': mongo_password,
        })

    client = motor.motor_asyncio.AsyncIOMotorClient(mongo_conf.mongo_url, **auth_kwargs)
    db = client.get_default_database()
    return db.tg_identities


@contextlib.asynccontextmanager
async def make_app(ctx: spydi.context.DependencyContext):
    def impl(routes, middlewares):
        app = aiohttp.web.Application(middlewares=middlewares)
        app.add_routes(routes)
        return app

    factory = spydi.factory.DependencyFactory(ctx)
    try:
        with factory.create_ctx(impl) as app:
            yield app
    except spydi.factory.ConstructionException as ex:
        print(ex.trace())


async def run_app(conf: config.AppConfig, port):
    ctx = make_context(conf)
    async with make_app(ctx) as app:
        await aiohttp.web._run_app(app, port=port)
