import os

import pymongo
import pytest

import motor.motor_asyncio

from yawf.identification.telegram import config, di


@pytest.fixture
def app_config(load_json, locate_file):
    return config.parse_app_config(load_json('config.json'),
                                   os.path.dirname(locate_file('config.json')))


@pytest.fixture
def app_context(app_config):
    return di.make_context(app_config)


@pytest.fixture
async def app(app_context, aiohttp_client):
    async with di.make_app(app_context) as application:
        yield await aiohttp_client(application)


@pytest.fixture(autouse=True)
async def mongo(app_config):
    client = motor.motor_asyncio.AsyncIOMotorClient(app_config.mongo_config.mongo_url)
    db = client.get_default_database()
    collection = db.tg_identities
    await collection.create_index([('tg_id', pymongo.ASCENDING)], unique=True)
    yield collection
    await db.drop_collection('tg_identities')
    assert not await collection.find_one()
