import datetime

import bson
import jwt
import pytest

from yawf.identification.telegram.handler import oauth


@pytest.mark.parametrize(
    'body, status, code',
    [
        (b'qwerty', 400, 'MALFORMED_TELEGRAM_CLAIMS'),
        ({}, 400, 'MALFORMED_TELEGRAM_CLAIMS'),
        ({'hash': '123'}, 400, 'MALFORMED_TELEGRAM_CLAIMS'),
        ({'id': '123', 'username': 'qwe', 'hash': '123'},
         400, 'MALFORMED_TELEGRAM_CLAIMS'),
        ({'id': '123', 'first_name': 'first', 'last_name': 'last', 'username': 'user',
          'auth_date': 'not-a-timestamp', 'hash': '123'}, 400, 'MALFORMED_TELEGRAM_CLAIMS'),
        ({'id': '123', 'first_name': 'first', 'last_name': 'last', 'username': 'user',
          'auth_date': 0, 'hash': '123'}, 403, 'INVALID_TELEGRAM_CLAIMS')
    ],
    ids=[
        'not a json',
        'empty json',
        'hash only',
        'some fields',
        'string timestamp',
        'wrong hash',
    ]
)
async def test_bad_request(app, body, status, code):
    if isinstance(body, bytes):
        response = await app.post('/oauth/telegram', data=body)
    else:
        response = await app.post('/oauth/telegram', json=body)

    assert response.status == status
    payload = await response.json()
    assert payload['code'] == code


async def test_use_old_credentials(app, mongo):
    payload = {'id': '123', 'first_name': 'first', 'last_name': 'last', 'username': 'user',
               'auth_date': 0}
    payload['hash'] = oauth.calculate_hash(payload, b'1234')

    await mongo.insert_one(
        {'tg_id': '123',
         'tg_username': 'user',
         'last_auth': datetime.datetime.fromtimestamp(10, datetime.timezone.utc)}
    )
    response = await app.post('/oauth/telegram', json=payload)
    assert response.status == 403
    body = await response.json()
    assert body['code'] == 'DUPLICATE_AUTHENTICATION_ATTEMPT'


async def test_reuse_id(app, mongo, load_data):
    payload = {'id': '123', 'first_name': 'first', 'last_name': 'last', 'username': 'user',
               'auth_date': 50}
    payload['hash'] = oauth.calculate_hash(payload, b'1234')

    credentials_id = bson.ObjectId()

    await mongo.insert_one(
        {'_id': credentials_id,
         'tg_id': '123',
         'tg_username': 'user',
         'last_auth': datetime.datetime.fromtimestamp(10, datetime.timezone.utc)}
    )

    response = await app.post('/oauth/telegram', json=payload)
    assert response.status == 200

    doc = await mongo.find_one({'tg_id': '123'})
    assert doc['last_auth'] == datetime.datetime.utcfromtimestamp(50)

    body = await response.json()
    token = body.pop('idToken')
    assert body == {
        'expiresIn': 3600
    }

    claims = jwt.decode(token, key=load_data('tg.pub'))
    assert claims.pop('jti')
    exp = datetime.datetime.fromtimestamp(claims.pop('exp'), datetime.timezone.utc)
    assert abs((exp - datetime.datetime.now(datetime.timezone.utc)).total_seconds() - 3600) < 1.0
    assert claims == {
        'iss': 'urn:yawf:identification:telegram',
        'sub': '**',
        'act': {'*': ['*']},
        'yawf.io/externalDisplayName': 'first last',
        'yawf.io/externalId': 'tg+id://' + str(credentials_id),
    }



async def test_success(app, mongo, load_data):
    payload = {'id': '123', 'first_name': 'first', 'last_name': 'last', 'username': 'user',
               'auth_date': 0}
    payload['hash'] = oauth.calculate_hash(payload, b'1234')

    response = await app.post('/oauth/telegram', json=payload)
    assert response.status == 200

    doc = await mongo.find_one({'tg_id': '123'})

    body = await response.json()
    token = body.pop('idToken')
    assert body == {
        'expiresIn': 3600
    }

    claims = jwt.decode(token, key=load_data('tg.pub'))
    assert claims.pop('jti')
    exp = datetime.datetime.fromtimestamp(claims.pop('exp'), datetime.timezone.utc)
    assert abs((exp - datetime.datetime.now(datetime.timezone.utc)).total_seconds() - 3600) < 1.0
    assert claims == {
        'iss': 'urn:yawf:identification:telegram',
        'sub': '**',
        'act': {'*': ['*']},
        'yawf.io/externalDisplayName': 'first last',
        'yawf.io/externalId': 'tg+id://' + str(doc['_id']),
    }
