aiohttp[speedups]==3.6.2
aiohttp==3.6.2
cryptography==2.8
dnspython==2.0.0
motor==2.1.0
PyJWT==1.7.1
spy-di==0.4.0
yawf-py-utils==0.2.0