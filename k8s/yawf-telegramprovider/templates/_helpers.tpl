{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "yawf-telegramprovider.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "yawf-telegramprovider.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "yawf-telegramprovider.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "yawf-telegramprovider.labels" -}}
helm.sh/chart: {{ include "yawf-telegramprovider.chart" . }}
{{ include "yawf-telegramprovider.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: yawf
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "yawf-telegramprovider.selectorLabels" -}}
app.kubernetes.io/name: {{ include "yawf-telegramprovider.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: yawf
{{- end -}}


{{- define "yawf-telegramprovider.mongoAuthEnv" }}
{{- with .Values.mongo.auth }}
- name: MONGO_USER
  valueFrom:
    secretKeyRef:
      name: {{ .secretName }}
      key: {{ default "user" .userKey }}
- name: MONGO_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .secretName }}
      key: {{ default "password" .passwordKey }}
{{- end -}}
{{- end -}}

{{- define "yawf-telegramprovider.initCmdAuth" -}}
{{- if .Values.mongo.auth -}}
-u `echo $MONGO_USER` -p `echo $MONGO_PASSWORD`
{{- else -}}
{{- end -}}
{{- end -}}

{{- define "yawf-telegramprovider.initCmd" -}}
mongo {{ .Values.mongo.addr | quote }} {{ include "yawf-telegramprovider.initCmdAuth" . }} /mongo-init/01-setup-index.js
{{- end -}}

{{- define "yawf-telegramprovider.imageTag" -}}
{{- default .Chart.AppVersion .Values.image.tag }}
{{- end -}}

{{- define "yawf-telegramprovider.image" -}}
{{ .Values.image.repository }}:{{ include "yawf-telegramprovider.imageTag" . }}
{{- end -}}