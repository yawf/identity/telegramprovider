import React from 'react'
import TelegramLoginButton from 'react-telegram-login'

const handleTelegramResponse = (
  tgClaim,
  apiUrl,
  apiTimeout,
  onIdentificationStarted,
  onIdentified,
  onIdentificationFailed
) => {
  onIdentificationStarted()

  const url = apiUrl + '/oauth/telegram'
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(tgClaim)
  })
    .then((response) => {
      if (!response.ok) {
        throw Error('Malformed Telegram claims')
      }

      return response.json()
    })
    .then((responseData) => {
      onIdentified(responseData.idToken, responseData.expiresIn)
    })
    .catch((err) => onIdentificationFailed(err))
}

export const TelegramOAuthIdentification = ({
  botName,
  apiUrl,
  apiTimeout,
  onIdentificationStarted,
  onIdentified,
  onIdentificationFailed
}) => {
  return (
    <TelegramLoginButton
      dataOnauth={(tgClaims) =>
        handleTelegramResponse(
          tgClaims,
          apiUrl,
          apiTimeout,
          onIdentificationStarted,
          onIdentified,
          onIdentificationFailed
        )
      }
      botName={botName}
    />
  )
}
