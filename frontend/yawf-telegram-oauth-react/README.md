# yawf-telegram-oauth-react

> Telegram OAuth Login widget for React (YAWF)

[![NPM](https://img.shields.io/npm/v/yawf-telegram-oauth-react.svg)](https://www.npmjs.com/package/yawf-telegram-oauth-react) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save yawf-telegram-oauth-react
```

## Usage

```jsx
import React from 'react'

import { TelegramOAuthIdentification } from 'yawf-telegram-oauth-react'

const App = () => {
  return <TelegramOAuthIdentification
    botName="MaiMeetMeUpBot"
    apiUrl=""
    apiTimeout={5.0}
    onIdentificationStarted={() =>{}}
    onIdentified={(idToken, expiresIn) => console.log(idToken, expiresIn)}
    onIdentificationFailed={() =>{}}
  />
}

export default App

```

## License

Apache 2.0 © [madmax_inc](https://gitlab.com/madmax_inc)
