import React from 'react'

import { TelegramOAuthIdentification } from 'yawf-telegram-oauth-react'

const App = () => {
  return <TelegramOAuthIdentification
    botName="MaiMeetMeUpBot"
    apiUrl=""
    apiTimeout={5.0}
    onIdentificationStarted={() =>{}}
    onIdentified={(idToken, expiresIn) => console.log(idToken, expiresIn)}
    onIdentificationFailed={() =>{}}
  />
}

export default App
